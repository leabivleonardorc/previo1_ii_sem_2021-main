/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisis_algoritmo;

import java.util.Scanner;

/**
 *
 * @author COLOQUE ACÁ SUS NOMBRES COMPLETOS
 */
public class Previo1 {

    public Previo1(){

    }

    public static void main(String[] args) {
        /*char vectorFizzBuzz[] = leerCodigoFizz();
        imprimirVector(vectorFizzBuzz);
        System.out.println("El código fizzBuzz es:" + getNumeroFizzBuzzz(vectorFizzBuzz));
        */
        Previo1 cadena = new Previo1();
        char[] test = {'f', 'i', 'z', 'z','b', 'u', 'z', 'z', '2', '0', '7'};
        cadena.getNumeroFizzBuzzz(test);

    }

    public long getNumeroFizzBuzzz(char[] vectorFizzBuzz){
        int n = vectorFizzBuzz.length;
        long numeroFizzBuzzz = 0;
        long exp = 1;
        for(int i = n-1; i>=0; i--){
            int num = vectorFizzBuzz[i] - '0';
            if(num<10){
                if(esDivisible(num, 3) || esDivisible(num, 5)){
                    return -1;
                }else{
                    numeroFizzBuzzz += num*exp;
                    exp = exp*10;
                }
            }else if(vectorFizzBuzz[i]=='z' && (i-3)>=0){
                if(vectorFizzBuzz[i-1]=='z'){
                    if(vectorFizzBuzz[i-2]=='i' && vectorFizzBuzz[i-3]=='f'){
                        numeroFizzBuzzz += 3*exp;
                        exp = exp*10;
                        i=i-3;
                    }else if(vectorFizzBuzz[i-2]=='u' && vectorFizzBuzz[i-3]=='b'){
                        if((i-4)>=0 && vectorFizzBuzz[i-4]=='z'){
                            if((i-7)>=0 && vectorFizzBuzz[i-7]=='f' && vectorFizzBuzz[i-6]=='i' && vectorFizzBuzz[i-5]=='z'){
                                numeroFizzBuzzz += 15*exp;
                                exp = exp*10;
                                i=i-7;
                            }else{
                                return -1;
                            }
                        }else{
                            numeroFizzBuzzz += 5*exp;
                            exp = exp*10;
                            i=i-3;
                        }
                    }else{
                        return -1;
                    }
                }else{
                    return -1;
                }
            }else{
                return -1;
            }
        }

        return numeroFizzBuzzz;
    }
    public static boolean esDivisible(int numero, int divisor){
        return numero % divisor == 0 && numero != 0;
    }
    /*
    private static long getNumeroFizzBuzzz(char[] vectorFizzBuzz) {

        return 0;
    }

     */

    /**
     * **************************************************************
     */
    /**
     * *******************METODOS DE AYUDA PARA LECUTURA DE DATOS*******************************************
     */
    /**
     * *******************NO DEBEN SER MODIFICADOS******************
     */
    /**
     * **************************************************************
     */
    /**
     * Esté método lee la cadena, NO HAY QUE CALCULARLE SU COSTO
     *
     * @return un vector de char con un código fizz buzz
     */
    /*
    private static char[] leerCodigoFizz() {
        Scanner in = new Scanner(System.in);
        limpiar();
        System.out.println("Por favor digite una cadena de fizzbuzz:");
        String cadena = in.nextLine().toLowerCase();
        return cadena.toCharArray();
    }

    private static void imprimirVector(char v[]) {
        limpiar();
        System.out.println("Vector de códigos:");
        for (char dato : v) {
            System.out.print(dato + "\t");
        }
        System.out.println("");
    }

    private static void limpiar() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

     */
}
